package Exceptions;

public class CarAlreadyExistsException extends Exception {
    //CarAlreadyExistsException выбрасывается, если автомобиль уже существует в коллекции
    public CarAlreadyExistsException(String message) {
        super(message);
    }
}
