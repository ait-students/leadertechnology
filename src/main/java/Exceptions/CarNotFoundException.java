package Exceptions;

public class CarNotFoundException extends Exception {
    //CarNotFoundException выбрасывается когда автомобиль не найден в коллекции.

    public CarNotFoundException(String message) {
        super(message);
    }
}