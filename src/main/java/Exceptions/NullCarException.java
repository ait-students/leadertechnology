package Exceptions;

public class NullCarException extends Exception {
    //NullCarException: выбрасывается, если переданный автомобиль является null
    public NullCarException(String message) {
        super(message);
    }
}
