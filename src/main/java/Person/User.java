package Person;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class User extends Person {
    private static final Logger LOGGER = LoggerFactory.getLogger(User.class);


       // private HashSet<Auto> purchases = new HashSet<>();   // Должен быть уникален для каждого объекта


        // статус юзер и статус аккаунта сразу по умолчанию определяем как User и Aktiv
       private UserAccountStatus accountStatus = UserAccountStatus.ACTIVE;

       public UserAccountStatus getAccountStatus() {
        return accountStatus;
    }

    public User(String name, String password, String phoneNumber, String eMail, int id, AccessLevel accessLevel, LocalDate dateOfCreate, UserAccountStatus accountStatus) {
        super(name, password, phoneNumber, eMail, id, accessLevel, dateOfCreate);
        this.accountStatus = accountStatus;
    }


    /*
        public HashSet<Auto> getPurchases() {
            return purchases;
        } */

    // Управление статусом аккаунта пользователя
        public void setAccountStatus(UserAccountStatus accountStatus) {
            this.accountStatus = accountStatus;
        }

        public void changeUserAccountStatus(UserAccountStatus status) {
            this.setAccountStatus(status);
            System.out.println("The relevant status is " + status);
        }



    }





