package Person;

import java.time.LocalDate;
import java.util.Objects;

public class Person {
    private String name;

    private String password;

    private String phoneNumber;

    private String email;

    private final int id; //неизменяемый параметр

    private AccessLevel accessLevel;

    //Дата автоматически инициализируется для каждого нового объекта текущей датой при создании
    private LocalDate dateOfCreate= LocalDate.now();


    //private HashSet<Message> messages = new HashSet<>();


    public Person(String name, String password, String phoneNumber, String email, int id, AccessLevel accessLevel, LocalDate dateOfCreate) {
        this.name = name;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.id = id;
        this.accessLevel = accessLevel;
        this.dateOfCreate = dateOfCreate;
    }

    // пустой конструктор для Тестирования
    public Person(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public LocalDate getDateOfCreate() {
        return dateOfCreate;
    }
    /*public HashSet<Message> getMessages() {
        return messages;
    }*/

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    //id и email уникальны
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id && Objects.equals(email, person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, id);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", eMail='" + email + '\'' +
                ", id=" + id +
                ", accessLevel=" + accessLevel +
                '}';
    }

}
