package Person;

public enum UserAccountStatus {
    // позволяет Админу управлять статусами аккаунтов
    ACTIVE ("Active account"),
    DELETED("Deleted account");

    String description;

    UserAccountStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
