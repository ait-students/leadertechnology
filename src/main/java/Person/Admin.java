package Person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Admin {
    private static final Logger LOGGER = LoggerFactory.getLogger(Admin.class);

    static ArrayList<User> users = new ArrayList<>(); //Для хранения юзеров


    //Метод для добавления пользователей
    public static ArrayList<User> addUser(User user) {
        LOGGER.info("New User registration:");
        if (user == null) {
            // выводим сообщение об ошибке (unchecked)
            LOGGER.info("Object user is null");
            throw new NullPointerException();
        } else {
            users.add(user);
            LOGGER.info("User " + user.getName() + " was added");
        }
        return users;
    }


    //Поиск пользователя по name
    public static User findUserByName(String nameToFind) {
        // Проверяем, что имя не null и не пустое
        if (nameToFind == null || nameToFind.trim().isEmpty()) {
            LOGGER.info("The Name is empty or null.");
            throw new IllegalArgumentException("Name cannot be null or empty");
        }
        // Ищем пользователя по имени в хранилище users
        for (User user : users) {
            if (user.getName().equalsIgnoreCase(nameToFind.trim())) {
                LOGGER.info("User {} was found.", user);
                return user;
            }
        }
        return null;
    }

        // Метод для удаления пользователя по имени
        public static User removeUserByName(String nameToRemove) {
                LOGGER.info("Remove user by name:");
            if (nameToRemove == null || nameToRemove.trim().isEmpty()) {
                LOGGER.info("The Name is empty or null.");
                throw new NullPointerException ("Name cannot be null or empty");
            }
        for (User userToRemove : users) {
            if (userToRemove.equals(nameToRemove)) {
                LOGGER.info("User {} was removed from the List.", nameToRemove);
            } else {
                LOGGER.info("User {} was not found", nameToRemove);
            }
        }
            return null;
        }
}



