package Person;

public enum AccessLevel {
    READER(0, "Reader"),
    USER(1, "User"),
    ADMIN(2,"Admin");

    private final int level;
    private final String role;

    AccessLevel(int level, String role) {
        this.level = level;
        this.role = role;
    }

    public boolean adminAccess(){
        return this.level == ADMIN.level;
    }

    public boolean userAccess(){
        return this.level >= USER.level;
    }

    public boolean readerAccess(){
        return this.level >= READER.level;
    }

}
