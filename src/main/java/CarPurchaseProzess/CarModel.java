package CarPurchaseProzess;
import Car.Model;

public class CarModel {
    private Model model;
    private String color;
    private double price;

    public void Car(Model model, String color, double price) {
        this.model = model;
        this.color = color;
        this.price = price;
    }

    public Model getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model=" + model +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}
