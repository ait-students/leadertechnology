package CarPurchaseProzess;
import Car.CarConstructor;
import Person.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class PurchaseLogger {
    private static final String FILE_NAME = "purchase_logs.txt";
    // Метод для записи покупки за наличные
    public static void logCashPurchase(User buyer, CarConstructor car) {
        if (buyer == null || car == null) {
            throw new IllegalArgumentException("Ошибка: Покупатель или автомобиль не определены.");

        }

        String entry = "Покупка за наличные\n" +
                "Дата: " + getCurrentDateTime() + "\n" +
                "Покупатель: " + buyer.getName() + "\n" +
                "Автомобиль: " + formatCarDetails(car) + "\n\n";
        writeToFile(entry);
    }

    // Метод для записи покупки в кредит
    public static void logCreditPurchase(User buyer, CarConstructor car, double interestRate, int months) {
        if (buyer == null || car == null || interestRate <= 0 || months <= 0) {
            throw new IllegalArgumentException("Ошибка: Некорректные данные для записи покупки в кредит.");

        }

        String entry = "Покупка в кредит\n" +
                "Дата: " + getCurrentDateTime() + "\n" +
                "Покупатель: " + buyer.getName() + "\n" +
                "Автомобиль: " + formatCarDetails(car) + "\n" +
                "Процентная ставка: " + interestRate + "%\n" +
                "Срок кредита: " + months + " месяцев\n" +
                "Ежемесячный платеж: " + calculateMonthlyPayment(car.getPrice(), interestRate) + " евро\n\n";
        writeToFile(entry);
    }

    // Метод для записи информации в файл
    private static void writeToFile(String entry) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME, true))) {
            writer.write(entry);
        } catch (IOException e) {
            System.err.println("Ошибка записи в файл: " + e.getMessage());
        }
    }

    // Метод для получения текущей даты и времени
    private static String getCurrentDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.now().format(formatter);
    }

    // Метод для вычисления ежемесячного платежа
    protected static double calculateMonthlyPayment(int price, double interestRate) {
        double months = 0;
        return (price * (1 + interestRate / 100)) / months;
    }

    // Метод для форматирования данных автомобиля
    private static String formatCarDetails(CarConstructor car) {
        return String.format("Модель: \nВерсия: \nЦвет: \nПробег:  км\nЦена:  евро\nГод выпуска: ",
                car.getModel(),
                car.getVersion(),
                car.getColor(),
                car.getPrice(),
                car.getYear());
    }
}


