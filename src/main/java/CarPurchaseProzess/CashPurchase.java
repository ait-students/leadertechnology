package CarPurchaseProzess;
import Person.User;
import Car.CarConstructor;
public class CashPurchase {
    public static void processPurchase(User buyer, CarConstructor car) {
        if (buyer == null || car == null) {
            throw new IllegalArgumentException("Покупатель или автомобиль не могут быть null.");
        }
        System.out.println("Покупка автомобиля за наличные:");
        System.out.println(car);
        System.out.println("Покупатель: " + buyer.getName());

        // Логирование покупки
        PurchaseLogger.logCashPurchase(buyer, car);
    }
}
