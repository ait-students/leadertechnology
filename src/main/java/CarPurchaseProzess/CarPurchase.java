package CarPurchaseProzess;
import Person.User;
import Car.CarConstructor;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CarPurchase {
    public static void processPurchase(User buyer, CarConstructor car) {
        // Проверка на null
        if (buyer == null || car == null) {
            System.err.println("Error: Buyer or car information is missing.");
            return;
        }

        // Логирование информации о покупке в консоль
        System.out.println("Покупка автомобиля за наличные:");
        System.out.println(car);
        System.out.println("Покупатель: " + buyer.getName());

        // Логирование покупки в файл
        logCashPurchase(buyer, car);
    }

    private static void logCashPurchase(User buyer, CarConstructor car) {
        String logEntry = "Покупка автомобиля за наличные:\n" +
                "Покупатель: " + buyer.getName() + "\n" +
                "Автомобиль: " + car + "\n" +
                "Дата: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n" +
                "--------------------------------------------------\n";

        // Запись логов в файл
        try (FileWriter writer = new FileWriter("purchase_log.txt", true)) {
            writer.write(logEntry);
        } catch (IOException e) {
            System.err.println("Failed to write purchase log to file: " + e.getMessage());
        }
    }
}



