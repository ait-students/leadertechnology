package CarPurchaseProzess;
import Person.User;
import Car.CarConstructor;
public class CreditPurchase {
    public static void processPurchase(User buyer, CarConstructor car, double interestRate, int months) {
        double monthlyPayment = (car.getPrice() * (1 + interestRate / 100)) / months;
        if (buyer == null || car == null || interestRate <= 0 || months <= 0) {
            throw new IllegalArgumentException("Недопустимые параметры покупки в кредит.");
        }

        System.out.println("Покупка автомобиля в кредит:");
        System.out.println("Автомобиль: " + car);
        System.out.println("Покупатель: " + buyer.getName());
        System.out.println("Процентная ставка: " + interestRate + "%");
        System.out.println("Срок кредита: " + months + " месяцев");
        System.out.println("Ежемесячный платеж: " + monthlyPayment + " евро");

        // Логирование покупки
        PurchaseLogger.logCreditPurchase(buyer, car, interestRate, months);
    }
}

