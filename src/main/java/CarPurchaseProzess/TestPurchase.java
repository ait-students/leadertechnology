package CarPurchaseProzess;
import Car.*;
import java.time.LocalDate;
import Person.*;
public class TestPurchase {
    public static void main(String[] args) {
        // Пример создания пользователя и автомобиля
        User user = new User("Ivan", "password123", "777555888", "qwerty@gmail.com",
                1, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        CarConstructor car = new CarConstructor(Model.ModelS, Version.LongRangeAWD,2000, Color.BLUE,19000,1975);

        // Обработка покупки
        CarPurchase.processPurchase(user, car);
    }
}
