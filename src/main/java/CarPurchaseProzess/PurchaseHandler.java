package CarPurchaseProzess;
import Person.User;
import  Car.CarConstructor;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PurchaseHandler {
    public static void handlePurchase(User buyer, CarConstructor car) {
        if (buyer == null || car == null) {
            throw new IllegalArgumentException("Покупатель или автомобиль не могут быть null.");
        }
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("Выберите способ покупки (1 - наличные, 2 - кредит): ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // consume the newline

            switch (choice) {
                case 1:
                    CashPurchase.processPurchase(buyer, car);
                    break;

                case 2:
                    System.out.println("Введите процентную ставку (%): ");
                    double interestRate = scanner.nextDouble();

                    if (interestRate <= 0) {
                        System.out.println("Процентная ставка должна быть положительной.");
                        return;
                    }

                    System.out.println("Введите срок кредита (месяцы): ");
                    int months = scanner.nextInt();

                    if (months <= 0) {
                        System.out.println("Срок кредита должен быть положительным.");
                        return;
                    }

                    CreditPurchase.processPurchase(buyer, car, interestRate, months);
                    break;

                default:
                    System.out.println("Неверный выбор. Пожалуйста, выберите 1 или 2.");
            }
        } catch (InputMismatchException e) {
            System.out.println("Ошибка ввода. Пожалуйста, введите корректные данные.");
            scanner.nextLine(); // consume the invalid input
        }
    }
}
