package CarPurchaseProzess;
import Person.User;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class Purchase<Car> {
    private User user;
    private Car car;
    private LocalDateTime purchaseDate;

    public Purchase(User user, Car car) {
        this.user = user;
        this.car = car;
        this.purchaseDate = LocalDateTime.now();
    }

    public void completePurchase() {
        // Вывод информации о покупке в консоль
        System.out.println("Purchase completed:");
        System.out.println("User: " + user.getName());
        System.out.println("Car: " + car);
        System.out.println("Date: " + purchaseDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        // Запись информации о покупке в файл
        try (FileWriter writer = new FileWriter("purchases.txt", true)) {
            writer.write("Purchase completed:\n");
            writer.write("User: " + user.getName() + "\n");
            writer.write("Car: " + car + "\n");
            writer.write("Date: " + purchaseDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
            writer.write("--------------------------------------------------\n");
        } catch (IOException e) {
            System.err.println("Failed to write purchase information to file: " + e.getMessage());
        }
    }

    public User getUser() {
        return user;
    }

    public Car getCar() {
        return car;
    }

    public LocalDateTime getPurchaseDate() {
        return purchaseDate;
    }
}
