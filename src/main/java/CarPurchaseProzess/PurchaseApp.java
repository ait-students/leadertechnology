package CarPurchaseProzess;
import Exceptions.CarAlreadyExistsException;
import Exceptions.NullCarException;
import Person.*;
import Car.*;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import static CarPurchaseProzess.PurchaseLogger.calculateMonthlyPayment;


public class PurchaseApp {
    private static String address;  // Переменная для хранения адреса

    public static void main(String[] args) throws Exception {
        DirectoryManagement directoryManagement = new DirectoryManagement();
        Scanner scanner = new Scanner(System.in);

        // Добавление автомобилей в каталог
        addCarsToCatalog(directoryManagement);

        // Печать каталога автомобилей
        try {
            directoryManagement.displayAutoCatalog();
        } catch (Exception e) {
            System.out.println("Ошибка при отображении каталога автомобилей: " + e.getMessage());
        }

        // Аутентификация пользователя и заполнение анкеты
        User buyer = authenticateUser(scanner);

        // Запрос адреса покупателя
        fillBuyerAddress(scanner);

        // Выбор автомобиля
        CarConstructor chosenCar = chooseCar(directoryManagement, scanner);

        // Оформление покупки
        handlePurchase(buyer, chosenCar, scanner, directoryManagement);

        scanner.close();
    }

    // Добавление автомобилей в каталог
    private static void addCarsToCatalog(DirectoryManagement directoryManagement) {
        try {
            directoryManagement.addNewCar(new CarConstructor(Model.ModelS, Version.Performance, 100, Color.BLUE, 139990, 2023));
            directoryManagement.addNewCar(new CarConstructor(Model.Model3, Version.LongRange, 250, Color.RED, 59990, 2024));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelX, Version.Plaid, 50, Color.BLACK, 149990, 2024));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelY, Version.LongRangeAWD, 150, Color.WHITE, 79990, 2024));
        } catch (CarAlreadyExistsException | NullCarException e) {
            System.out.println("Ошибка при добавлении автомобиля в каталог: " + e.getMessage());
        }
    }

    // Аутентификация пользователя и заполнение анкеты
    private static User authenticateUser(Scanner scanner) {
        System.out.println("=== Вход в систему ===");
        String email = inputWithValidation(scanner, "Введите ваш email:", "Некорректный email. Введите корректный email:",
                input -> input.matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$"));
        String password = inputWithValidation(scanner, "Введите ваш пароль:", "Пароль должен быть не менее 6 символов.",
                input -> input.length() >= 6);

        System.out.println("=== Заполнение анкеты покупателя ===");
        String name = inputWithValidation(scanner, "Введите ваше имя:", "Имя не должно быть пустым.",
                input -> !input.trim().isEmpty());
        String phone = inputWithValidation(scanner, "Введите ваш телефон:", "Некорректный телефон. Введите корректный телефонный номер:",
                input -> input.matches("^(\\+\\d{1,3})?\\d{10,15}$")); // Проверка телефона

        // Возврат пользователя с минимальной информацией
        return new User(name, password, phone, email, 1, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
    }

    // Заполнение адреса покупателя
    private static void fillBuyerAddress(Scanner scanner) {
        System.out.println("=== Ввод адреса покупателя ===");
        address = inputWithValidation(scanner, "Введите ваш адрес:", "Адрес не должен быть пустым.",
                input -> !input.trim().isEmpty());
    }

    // Выбор автомобиля с повторным выбором при неудаче
    private static CarConstructor chooseCar(DirectoryManagement directoryManagement, Scanner scanner) throws Exception {
        CarConstructor chosenCar = null;
        while (chosenCar == null) {
            System.out.println("=== Выбор автомобиля ===");
            Model model = null;
            try {
                model = Model.valueOf(inputWithValidation(scanner, "Введите модель автомобиля (ModelS, Model3, ModelX, ModelY):",
                        "Неверно введена модель автомобиля. Доступные модели: ModelS, Model3, ModelX, ModelY.",
                        PurchaseApp::isValidModel));
            } catch (IllegalArgumentException e) {
                System.out.println("Ошибка: Модель не найдена. Пожалуйста, попробуйте еще раз.");
                continue;
            }

            HashSet<CarConstructor> availableCars = directoryManagement.searchAllAutosByModel(model);

            Version version = null;
            try {
                version = Version.valueOf(inputWithValidation(scanner, "Введите версию автомобиля (Performance, LongRange, Plaid, LongRangeAWD):",
                        "Неверно введена версия автомобиля. Доступные версии: Performance, LongRange, Plaid, LongRangeAWD.",
                        PurchaseApp::isValidVersion));
            } catch (IllegalArgumentException e) {
                System.out.println("Ошибка: Версия не найдена. Пожалуйста, попробуйте еще раз.");
                continue;
            }

            Version finalVersion = version;
            chosenCar = availableCars.stream()
                    .filter(car -> car.getVersion().equals(finalVersion))
                    .findFirst()
                    .orElse(null);

            if (chosenCar == null) {
                System.out.println("Автомобиль не найден. Пожалуйста, попробуйте еще раз.");
            }
        }
        return chosenCar;
    }

    // Оформление покупки
    private static void handlePurchase(User buyer, CarConstructor chosenCar, Scanner scanner, DirectoryManagement directoryManagement) throws Exception {
        while (true) {
            System.out.println("=== Оформление покупки ===");
            String paymentType = inputWithValidation(scanner, "Выберите тип оплаты (наличные/кредит):",
                    "Некорректный выбор. Введите 'наличные' или 'кредит'.",
                    input -> input.equalsIgnoreCase("наличные") || input.equalsIgnoreCase("кредит"));

            double amount;
            int creditYears = 0;
            if (paymentType.equalsIgnoreCase("кредит")) {
                System.out.println("=== Оформление кредита ===");

                // Повторный запрос модели и версии автомобиля
                chosenCar = chooseCarForCredit(directoryManagement, scanner);

                creditYears = inputIntWithValidation(scanner, "Введите срок кредита (лет):", "Некорректный ввод. Введите положительное число.",
                        value -> value > 0);

                amount = calculateMonthlyPayment(chosenCar.getPrice(), creditYears);
                System.out.printf("Месячный платеж: %.2f рублей\n", amount);
            } else {
                amount = chosenCar.getPrice();
                System.out.printf("Сумма к оплате: %.2f рублей\n", amount);
            }

            // Подтверждение покупки
            if (confirmPurchase(buyer, chosenCar, paymentType, amount, creditYears, scanner)) {
                savePurchaseInfo(buyer, chosenCar, paymentType, amount, creditYears);
                System.out.println("=== Поздравляем с покупкой! ===");
                displayPurchaseInfo(buyer, chosenCar, paymentType, amount, creditYears);
                break;
            } else {
                System.out.println("Повторное оформление покупки...");
            }
        }
    }

    // Выбор автомобиля для кредита
    private static CarConstructor chooseCarForCredit(DirectoryManagement directoryManagement, Scanner scanner) throws Exception {
        CarConstructor chosenCar = null;
        while (chosenCar == null) {
            System.out.println("=== Выбор автомобиля для кредита ===");
            Model model = null;
            try {
                model = Model.valueOf(inputWithValidation(scanner, "Введите модель автомобиля для кредита (ModelS, Model3, ModelX, ModelY):",
                        "Неверно введена модель автомобиля. Доступные модели: ModelS, Model3, ModelX, ModelY.",
                        PurchaseApp::isValidModel));
            } catch (IllegalArgumentException e) {
                System.out.println("Ошибка: Модель не найдена. Пожалуйста, попробуйте еще раз.");
                continue;
            }

            HashSet<CarConstructor> availableCars = directoryManagement.searchAllAutosByModel(model);

            Version version = null;
            try {
                version = Version.valueOf(inputWithValidation(scanner, "Введите версию автомобиля для кредита (Performance, LongRange, Plaid, LongRangeAWD):",
                        "Неверно введена версия автомобиля. Доступные версии: Performance, LongRange, Plaid, LongRangeAWD.",
                        PurchaseApp::isValidVersion));
            } catch (IllegalArgumentException e) {
                System.out.println("Ошибка: Версия не найдена. Пожалуйста, попробуйте еще раз.");
                continue;
            }

            Version finalVersion = version;
            chosenCar = availableCars.stream()
                    .filter(car -> car.getVersion().equals(finalVersion))
                    .findFirst()
                    .orElse(null);

            if (chosenCar == null) {
                System.out.println("Автомобиль не найден. Пожалуйста, попробуйте еще раз.");
            }
        }
        return chosenCar;
    }

    // Подтверждение покупки пользователем
    private static boolean confirmPurchase(User buyer, CarConstructor chosenCar, String paymentType, double amount, int creditYears, Scanner scanner) {
        displayPurchaseInfo(buyer, chosenCar, paymentType, amount, creditYears);
        String confirmation = inputWithValidation(scanner, "Подтвердите покупку (да/нет):",
                "Некорректный ввод. Введите 'да' или 'нет'.",
                input -> input.equalsIgnoreCase("да") || input.equalsIgnoreCase("нет"));
        return confirmation.equalsIgnoreCase("да");
    }

    // Отображение информации о покупке
    private static void displayPurchaseInfo(User buyer, CarConstructor chosenCar, String paymentType, double amount, int creditYears) {
        System.out.println("=== Информация о покупке ===");
        System.out.printf("Покупатель: %s\n", buyer.getName());
        System.out.printf("Адрес: %s\n", address); // Добавлен адрес
        System.out.printf("Телефон: %s\n", buyer.getPhoneNumber());
        System.out.printf("Автомобиль: %s %s %s\n", chosenCar.getModel(), chosenCar.getVersion(), chosenCar.getColor());
        System.out.printf("Год: %d\n", chosenCar.getYear());
        System.out.printf("Цена: %.2f\n", chosenCar.getPrice());
        System.out.printf("Тип оплаты: %s\n", paymentType);
        if (paymentType.equalsIgnoreCase("кредит")) {
            System.out.printf("Срок кредита: %d лет\n", creditYears);
            System.out.printf("Месячный платеж: %.2f рублей\n", amount);
        } else {
            System.out.printf("Оплачено: %.2f рублей\n", amount);
        }
    }

    // Запись информации о покупке в файл
    private static void savePurchaseInfo(User buyer, CarConstructor chosenCar, String paymentType, double amount, int creditYears) {
        String filename = "purchase_info.txt";
        try (FileWriter writer = new FileWriter(filename, true)) {
            writer.write(String.format("Покупатель: %s\n", buyer.getName()));
            writer.write(String.format("Адрес: %s\n", address)); // Добавлен адрес
            writer.write(String.format("Телефон: %s\n", buyer.getPhoneNumber()));
            writer.write(String.format("Автомобиль: %s %s %s\n", chosenCar.getModel(), chosenCar.getVersion(), chosenCar.getColor()));
            writer.write(String.format("Год: %d\n", chosenCar.getYear()));
            writer.write(String.format("Цена: %.2f\n", chosenCar.getPrice()));
            writer.write(String.format("Тип оплаты: %s\n", paymentType));
            if (paymentType.equalsIgnoreCase("кредит")) {
                writer.write(String.format("Срок кредита: %d лет\n", creditYears));
                writer.write(String.format("Месячный платеж: %.2f рублей\n", amount));
            } else {
                writer.write(String.format("Оплачено: %.2f рублей\n", amount));
            }
            writer.write("===\n");
        } catch (IOException e) {
            System.out.println("Ошибка при записи информации о покупке: " + e.getMessage());
        }
    }

    // Расчет месячного платежа
    private static double calculateMonthlyPayment(double price, int years) {
        double interestRate = 0.05; // Процентная ставка 5%
        int months = years * 12;
        double monthlyRate = interestRate / 12;
        return price * monthlyRate / (1 - Math.pow(1 + monthlyRate, -months));
    }

    // Проверка допустимости модели
    private static boolean isValidModel(String model) {
        try {
            Model.valueOf(model);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    // Проверка допустимости версии
    private static boolean isValidVersion(String version) {
        try {
            Version.valueOf(version);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    // Проверка допустимости цвета
    private static boolean isValidColor(String color) {
        try {
            Color.valueOf(color);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    // Валидация ввода строки
    private static String inputWithValidation(Scanner scanner, String prompt, String errorMsg, Validator<String> validator) {
        System.out.println(prompt);
        String input = scanner.nextLine().trim();
        while (!validator.validate(input)) {
            System.out.println(errorMsg);
            input = scanner.nextLine().trim();
        }
        return input;
    }

    // Валидация ввода целого числа
    private static int inputIntWithValidation(Scanner scanner, String prompt, String errorMsg, Validator<Integer> validator) {
        System.out.print(prompt);
        while (!scanner.hasNextInt()) {
            System.out.println(errorMsg);
            scanner.next(); // Очистка неправильного ввода
            System.out.print(prompt);
        }
        int value = scanner.nextInt();
        while (!validator.validate(value)) {
            System.out.println(errorMsg);
            while (!scanner.hasNextInt()) {
                System.out.println(errorMsg);
                scanner.next(); // Очистка неправильного ввода
                System.out.print(prompt);
            }
            value = scanner.nextInt();
        }
        scanner.nextLine(); // Очистка строки после ввода числа
        return value;
    }

    @FunctionalInterface
    interface Validator<T> {
        boolean validate(T input);
    }
}
