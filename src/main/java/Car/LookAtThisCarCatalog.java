package Car;

import java.util.HashSet;

public interface LookAtThisCarCatalog {

    //метод выводит на экран текущий каталог автомобилей
    void displayAutoCatalog() throws Exception;

    //метод ищет и возвращает все автомобили из каталога, соответствующие указанной версии
    HashSet<CarConstructor> searchAllAutosByVersion(Version version) throws Exception;

    //метод ищет и возвращает все автомобили из каталога, соответствующие указанной модели
    HashSet<CarConstructor> searchAllAutosByModel(Model model) throws Exception;

    //метод ищет и возвращает все автомобили из каталога, цена которых находится в указанном диапазоне
    HashSet<CarConstructor> searchByPriceCategory(int minPrice, int maxPrice) throws Exception;

    //метод ищет и возвращает все автомобили из каталога, выпущенные в указанном году
    HashSet<CarConstructor> searchByYear(int year) throws Exception;

}