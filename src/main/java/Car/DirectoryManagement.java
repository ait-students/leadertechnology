package Car;

import Exceptions.CarAlreadyExistsException;
import Exceptions.CarNotFoundException;
import Exceptions.NullCarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.stream.Collectors;

//Класс управление каталогом//
public class DirectoryManagement implements LookAtThisCarCatalog {
    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryManagement.class);
    private HashSet<CarConstructor> collection = new HashSet<>();

    //метод для отображения полного каталога автомобилей, доступных в нашей коллекции
    @Override
    public void displayAutoCatalog() throws Exception {
        if (collection.isEmpty()) {
            throw new Exception("Auto catalog is empty");
        }
        collection.forEach(car -> LOGGER.info(car.toString()));
    }

    // Метод для поиска всех автомобилей определенной версии в коллекции
    public HashSet<CarConstructor> searchAllAutosByVersion(Version version) throws Exception {
        if (version == null) {
            //NullCarException: выбрасывается, если переданный автомобиль является null
            throw new NullCarException("Version cannot be null");
        }
        HashSet<CarConstructor> foundCars = collection.stream()
                .filter(car -> car.getVersion().equals(version))
                .collect(Collectors.toCollection(HashSet::new));

        if (foundCars.isEmpty()) {
            LOGGER.info("No cars found with version: " + version);

            throw new CarNotFoundException("No cars found with version: " + version);
        } else {
            LOGGER.info("Cars found with version: " + version);
            for (CarConstructor car : foundCars) {
                LOGGER.info(car.toString());
            }
        }
        return foundCars;
    }

    // Метод для поиска всех автомобилей определенной модели в коллекции
    @Override
    public HashSet<CarConstructor> searchAllAutosByModel(Model model) throws Exception {
        if (model == null) {
            throw new NullCarException("Model cannot be null");
        }
        HashSet<CarConstructor> foundCars = collection.stream()
                .filter(car -> car.getModel().equals(model))
                .collect(Collectors.toCollection(HashSet::new));

        if (foundCars.isEmpty()) {
            LOGGER.info("No cars found with model: " + model);
            throw new CarNotFoundException("No cars found with model: " + model);
        } else {
            LOGGER.info("Cars found with model: " + model);
            for (CarConstructor car : foundCars) {
                LOGGER.info(car.toString());
            }
        }

        return foundCars;
    }

    // Метод для поиска всех автомобилей определенной ценовой категории в коллекции
    @Override
    public HashSet<CarConstructor> searchByPriceCategory(int minPrice, int maxPrice) throws Exception {

        HashSet<CarConstructor> foundCars = new HashSet<>();

        for (CarConstructor car : collection) {
            if (car.getPrice() >= minPrice && car.getPrice() <= maxPrice) {
                foundCars.add(car);
            }
        }
        if (foundCars.isEmpty()) {
            LOGGER.info("No cars found in the price category " + minPrice + " - " + maxPrice);
            throw new CarNotFoundException("No cars found in the price category " + minPrice + " - " + maxPrice);
        } else {
            LOGGER.info("Cars found in the price category " + minPrice + " - " + maxPrice + ":");
            for (CarConstructor car : foundCars) {
                LOGGER.info(car.toString());
            }
        }
        return foundCars;
    }

    // Метод для поиска всех автомобилей по определенному году выпуска
    @Override
    public HashSet<CarConstructor> searchByYear(int year) throws Exception {
        if (year <= 0) {
            throw new IllegalArgumentException("Year cannot be negative or zero");
        }
        HashSet<CarConstructor> foundCars = collection.stream()
                .filter(car -> car.getYear() == year)
                .collect(Collectors.toCollection(HashSet::new));

        if (foundCars.isEmpty()) {
            LOGGER.info("No cars found from year: " + year);
            throw new CarNotFoundException("No cars found from year: " + year);
        } else {
            LOGGER.info("Cars found from year: " + year);
            for (CarConstructor car : foundCars) {
                LOGGER.info(car.toString());
            }
        }

        return foundCars;
    }

    // Метод добавления автомобиля в коллекцию
    public void addNewCar(CarConstructor carConstructor) throws CarAlreadyExistsException, NullCarException {
        if (carConstructor == null) {
            LOGGER.error("ERROR!!! Element is null");
            //NullCarException: выбрасывается, если переданный автомобиль является null
            throw new NullCarException("Car cannot be null");
        } else {
            boolean addResult = collection.add(carConstructor);
            if (addResult) {
                LOGGER.info("Car " + carConstructor.getModel() + " version " + carConstructor.getVersion() + " price "
                        + carConstructor.getPrice() + " - euro " + carConstructor.getYear() + " year " + " was added to collection ");
            } else {
                LOGGER.error("ERROR!!!! A car with such data already exists!");
                //CarAlreadyExistsException выбрасывается, если автомобиль уже существует в коллекции
                throw new CarAlreadyExistsException("A car with such data already exists!");
            }
        }
    }

    // Метод удаления автомобиля
    public void deleteCar(CarConstructor carConstructor) throws CarNotFoundException {
        boolean deleteResult = collection.remove(carConstructor);
        if (deleteResult) {
            LOGGER.info(" Car " + carConstructor.getModel() + " version " + carConstructor.getVersion() + " price "
                    + carConstructor.getPrice() + " - euro " + carConstructor.getYear() + " year " + " was removed from collection ");
        } else {
            LOGGER.error("ERROR!!! Car " + carConstructor.getModel() + " version " + carConstructor.getVersion() + " price "
                    + carConstructor.getPrice() + " - euro " + carConstructor.getYear() + " year " + " was not removed ");
            throw new CarNotFoundException("Car not found in the collection");
        }
    }

    // Печать всех моделей в коллекции
    public void printAllModelsInCollection() throws Exception {
        if (collection.isEmpty()) {
            throw new Exception("No models in collection");
        }
        LOGGER.info("-------Models in collection----------");
        HashSet<Model> models = new HashSet<>();
        for (CarConstructor carConstructor : collection) {
            models.add(carConstructor.getModel());
        }
        for (Model model : models) {
            LOGGER.info("Model: " + model); // Логирование каждой модели
        }
    }

    // Генерация и обработка проверяемых исключений
    public void generateIOException() throws IOException {
        throw new IOException("Simulated IOException");
    }

    public void generateSQLException() throws SQLException {
        throw new SQLException("Simulated SQLException");
    }

    public void generateClassNotFoundException() throws ClassNotFoundException {
        throw new ClassNotFoundException("Simulated ClassNotFoundException");
    }
}