package Car;

import Exceptions.CarAlreadyExistsException;
import Exceptions.CarNotFoundException;
import Exceptions.NullCarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class TestAutoCatalog {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestAutoCatalog.class);

    public static void main(String[] args) {

        DirectoryManagement directoryManagement = new DirectoryManagement();
        // Добавление автомобилей в каталог
        try {
            directoryManagement.addNewCar(new CarConstructor(Model.ModelS, Version.Performance, 100, Color.BLUE, 139990, 2023));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelS, Version.LongRange, 110, Color.RED, 109990, 2022));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelS, Version.Plaid, 110, Color.WHITE, 79990, 2022));
            directoryManagement.addNewCar(new CarConstructor(Model.Model3, Version.LongRangeAWD, 90, Color.BLACK, 107990, 2021));
            directoryManagement.addNewCar(new CarConstructor(Model.Model3, Version.Performance, 90, Color.SILVER, 85990, 2021));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelX, Version.LongRange, 100, Color.BLACK, 114990, 2020));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelX, Version.Plaid, 100, Color.SILVER, 123990, 2021));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelY, Version.LongRange, 90, Color.RED, 100990, 2021));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelY, Version.Performance, 90, Color.SILVER, 149990, 2022));
            directoryManagement.addNewCar(new CarConstructor(Model.ModelS, Version.Performance, 120, Color.BLUE, 139990, 2023));
        } catch (CarAlreadyExistsException | NullCarException exception) {
            LOGGER.error("Error adding new car", exception.getMessage());
        } catch (Exception exception) {
            LOGGER.error("Unexpected error", exception.getMessage());
        }
        System.out.println("___________________________");

        // Удаление объекта из каталога и вывод каталога
        try {
            directoryManagement.deleteCar(new CarConstructor(Model.ModelY, Version.LongRange, 90, Color.RED, 100990, 2027));
        } catch (CarNotFoundException exception) {
            LOGGER.error("Error deleting car", exception.getMessage());
        } catch (Exception exception) {
            LOGGER.error("Unexpected error", exception.getMessage());
        }
        try {
            directoryManagement.displayAutoCatalog();
        } catch (Exception exception) {
            LOGGER.error("Error displaying auto catalog", exception.getMessage());
        }
        System.out.println("___________________________");

        // Вывод результатов поиска всех автомобилей по версии
        try {
            Version searchVersion = Version.Plaid;
            HashSet<CarConstructor> foundVersion = directoryManagement.searchAllAutosByVersion(searchVersion);
            System.out.println("Found cars with version " + searchVersion + ":");
            for (CarConstructor car : foundVersion) {
                System.out.println(car);
            }
        } catch (Exception exception) {
            LOGGER.error("Error searching cars by version", exception.getMessage());
        }

        // Вывод результатов поиска всех автомобилей по модели
        try {
            Model searchModel = Model.ModelS;
            HashSet<CarConstructor> foundModel = directoryManagement.searchAllAutosByModel(searchModel);
            System.out.println("Found cars with model " + searchModel + ":");
            for (CarConstructor car : foundModel) {
                System.out.println(car);
            }
        } catch (Exception exception) {
            LOGGER.error("Error searching cars by model", exception.getMessage());
        }

        // Вывод результатов поиска автомобилей по году выпуска
        try {
            int searchYear = 2021;
            HashSet<CarConstructor> foundCarsByYear = directoryManagement.searchByYear(searchYear);
            System.out.println("Found cars released in year " + searchYear + ":");
            for (CarConstructor car : foundCarsByYear) {
                System.out.println(car);
            }
        } catch (Exception exception) {
            LOGGER.error("Error searching cars by year", exception.getMessage());
        }

        // Вывод результатов поиска автомобилей по ценовой категории
        try {
            int minPrice = 100000;
            int maxPrice = 150000;
            HashSet<CarConstructor> foundCars = directoryManagement.searchByPriceCategory(minPrice, maxPrice);
            if (foundCars.isEmpty()) {
                System.out.println("No cars found in the price category " + minPrice + " - " + maxPrice);
            } else {
                List<CarConstructor> sortedCars = new ArrayList<>(foundCars);
                sortedCars.sort(Comparator.comparingInt(CarConstructor::getPrice));

                System.out.println("Cars found in the price category " + minPrice + " - " + maxPrice + ", sorted by price:");
                for (CarConstructor car : sortedCars) {
                    System.out.println(car);
                }
            }
        } catch (Exception exception) {
            LOGGER.error("Error searching cars by price category", exception);
        }


        // Вывод списка всех моделей
        try {
            directoryManagement.printAllModelsInCollection();
        } catch (Exception exception) {
            LOGGER.info("Error printing all models in collection", exception.getMessage());
        }
        System.out.println("___________________________");

        // Генерация и обработка проверяемых исключений
        try {
            directoryManagement.generateIOException();
        } catch (IOException e) {
            LOGGER.info("Caught IOException: " + e.getMessage());
        }
        try {
            directoryManagement.generateSQLException();
        } catch (SQLException e) {
            LOGGER.info("Caught SQLException: " + e.getMessage());
        }
        try {
            directoryManagement.generateClassNotFoundException();
        } catch (ClassNotFoundException e) {
            LOGGER.info("Caught ClassNotFoundException: " + e.getMessage());
        }
    }
}
