package Car;

import java.util.Objects;

public class CarConstructor {
    private Model model;
    private Version version;
    private int battery;
    private Color color;
    private int price;
    private int year;
    public CarConstructor(Model model, Version version, int battery, Color color, int price, int year) {
        this.model = model;
        this.version = version;
        this.battery = battery;
        this.color = color;
        this.price = price;
        this.year = year;
    }
    public Model getModel() {
        return model;
    }

    public Version getVersion() {
        return version;
    }

    public int getBattery() {
        return battery;
    }

    public Color getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarConstructor that = (CarConstructor) o;
        return battery == that.battery && price == that.price && year == that.year && version == that.version && color == that.color;
    }

    @Override
    public int hashCode() {

        return Objects.hash(version, battery, color, price, year);
    }

    @Override
    public String toString() {
        return "model = '" + model + '\'' +
                ", version = '" + version + '\'' +
                ", battery = " + battery + " кВт/ч " +
                ", color = " + color +
                ", price = " + price +
                ", year = " + year +
                '}';
        }
    }
