package tests;
import Car.*;
import Person.User;
import CarPurchaseProzess.CashPurchase;
import CarPurchaseProzess.CreditPurchase;
import Person.AccessLevel;
import Person.UserAccountStatus;
import org.testng.annotations.Test;

import java.time.LocalDate;
public class PurchaseTests {
    @Test
    public void testCashPurchase() {
        User buyer = new User("Ivan", "password", "777555888", "qwerty@gmail.com", 1, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        CarConstructor car = new CarConstructor(Model.ModelS, Version.Performance, 100, Color.BLUE, 139990, 2023);
        CashPurchase.processPurchase(buyer,car );
        // Проверка вывода на консоль (можно сделать с помощью mock библиотеки или проверки в логах)
    }

    @Test
    public void testCreditPurchase() {
        User buyer = new User("Ivan", "password", "777555888", "qwerty@gmail.com", 1, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        CarConstructor car = new CarConstructor(Model.ModelS, Version.Performance, 100, Color.BLUE, 139990, 2023);
        CreditPurchase.processPurchase(buyer, car, 5.0, 24);
        // Проверка расчета кредита и вывода на консоль
    }
}
