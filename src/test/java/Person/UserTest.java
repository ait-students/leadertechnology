package Person;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserTest {
    // покрытие 100%

    private User user;

    @BeforeEach
    public void setUp(){
        user = new User("Ivan", "123rty", "777555888", "qwerty@gmail.com", 1,
                AccessLevel.USER, LocalDate.now(),UserAccountStatus.ACTIVE);
    }

    @Test
    void setAccountStatusAktiv(){
        UserAccountStatus expectedStatus = UserAccountStatus.ACTIVE;
        user.setAccountStatus(expectedStatus);
        assertEquals(expectedStatus, user.getAccountStatus());
    }

    @Test
    void setAccountStatusNull(){
        UserAccountStatus expectedStatus = null;
        user.setAccountStatus((expectedStatus));
        assertNull(user.getAccountStatus());
    }

    @Test
    void changeUserAccountStatusToDeletePositive(){
        UserAccountStatus expectedStatus = UserAccountStatus.DELETED;
        user.changeUserAccountStatus(expectedStatus);
        assertEquals(expectedStatus,user.getAccountStatus());
    }

}
