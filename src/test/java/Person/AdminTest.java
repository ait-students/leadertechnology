package Person;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import static Person.Admin.removeUserByName;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdminTest {

    private ArrayList<User> users;


    @BeforeEach
    void setUp() {
        users = new ArrayList<>();


    }

    @Test
    void testAddUserSuccess() {
        assertEquals(0, users.size()); // проверка листа на ноль
        User user = new User("Ivan", "123rty", "777555888",
                "qwerty@gmail.com", 1, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        ArrayList<User> result = Admin.addUser(user);
        assertEquals(1, result.size());
    }

    @Test
    public void testAddUserNull() {
        User nullUser = null;
        assertThrows(NullPointerException.class, () -> {
            Admin.addUser(null);
        });
    }

    @Test
    void testFindUserByNameNullPointerException(){
        User user = null;
        assertThrows(NullPointerException.class, () -> { Admin.findUserByName(null); });
    }
    @Test
    @Ignore("Метод работает некорректно!!! Проверить!")
    void testFindUserByNameSuccess(){
        User user = new User("Ivan", "123rty", "777555888",
                "qwerty@gmail.com", 20, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        User foundUser = Admin.findUserByName("Ivan");
        assertEquals("Ivan", Admin.findUserByName(foundUser.getName()));
    }

    @Test
    @Ignore("Метод работает некорректно!!! Проверить!")
    void removeUserByNameSuccess(){
      /*  User user = new User("Ivan", "123rty", "777555888",
               "qwerty@gmail.com", 20, AccessLevel.USER, LocalDate.now(), UserAccountStatus.ACTIVE);
        User user1 = new User("Nina", "333","67896789", "fds43@", 4,
                AccessLevel.USER, LocalDate.now(),UserAccountStatus.ACTIVE);v

       users.add(user1);
       users.add(user);v*/

       String removedUser = "Nina";
       // User removedUser = new User("Nina", "333","67896789", "fds43@", 4,
           //     AccessLevel.USER, LocalDate.now(),UserAccountStatus.ACTIVE);
        assertEquals(removedUser, Admin.removeUserByName("Nina"));
        //assertEquals("Nina", removedUser.getName());
       // assertEquals(2, users.size());
        assertFalse(users.contains(removedUser));


    }

    @Test
    public void testRemoveUserByNameNotFound() {

        String nonExistingName = "Alice";
        User removedUser = removeUserByName(nonExistingName);
        assertEquals(0, users.size());
    }


    @Test
    void removeUserByNameNullPointerException(){
        User user = null;
        assertThrows(NullPointerException.class, () -> { removeUserByName(null); });
    }


}
